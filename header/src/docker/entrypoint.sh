#!/usr/bin/env bash

set -e

if [[ "$HIDE_MAPFISHAPP" =~ ^(yes|true|y|1)$ ]]; then
  # comment the li blocks
  sed -ri 's/(^\s+<li.*key="viewer".*\/li>)/<!-- hidden --> <!--\1-->/' /var/lib/jetty/webapps/header/WEB-INF/jsp/index.jsp
fi

if [[ "$HIDE_MAPSTORE" =~ ^(yes|true|y|1)$ ]]; then
  # Comment the first li block. Apply it twice, to only comment the public links, not the admin links (in the end of the file)
  sed -ri '0, /(^\s+<li.*key="mapstore".*\/li>)/{s/(^\s+<li.*key="mapstore".*\/li>)/<!-- hidden --> <!--\1-->/}' /var/lib/jetty/webapps/header/WEB-INF/jsp/index.jsp
  sed -ri '0, /(^\s+<li.*key="mapstore".*\/li>)/{s/(^\s+<li.*key="mapstore".*\/li>)/<!-- hidden --> <!--\1-->/}' /var/lib/jetty/webapps/header/WEB-INF/jsp/index.jsp
fi


exec "$@"
