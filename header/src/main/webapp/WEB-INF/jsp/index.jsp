<%--

 Copyright (C) 2009-2018 by the geOrchestra PSC

 This file is part of geOrchestra.

 geOrchestra is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.

 geOrchestra is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along with
 geOrchestra.  If not, see <http://www.gnu.org/licenses/>.

--%>

<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="org.georchestra._header.Utf8ResourceBundle" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page isELIgnored="false" %>
<%
    Boolean anonymous = true;

/*
response.setDateHeader("Expires", 31536000);
response.setHeader("Cache-Control", "private, max-age=31536000");
*/

// to prevent problems with proxies, and for now:
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
    response.setHeader("Pragma", "no-cache"); // HTTP 1.0
    response.setDateHeader("Expires", 0); // Proxies.

    String active = request.getParameter("active");
    if (active == null) {
        active = "none";
    }

    Locale rLocale = request.getLocale();


    String detectedLanguage = rLocale.getLanguage();
    String forcedLang = request.getParameter("lang");
    if (forcedLang == null) {
        Map<String, Cookie> cookieMap = new HashMap<>();
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                cookieMap.put(cookie.getName(), cookie);
            }
        }
        if (cookieMap.get("geor-lang") != null) {
            forcedLang = cookieMap.get("geor-lang").getValue();
        }
    }
    String lang = (String) request.getAttribute("defaultLanguage");
    if (forcedLang != null) {
        if (forcedLang.equals("en") || forcedLang.equals("es") || forcedLang.equals("ru") ||
                forcedLang.equals("fr") || forcedLang.equals("de") || forcedLang.equals("nl") || forcedLang.equals("sk")) {
            lang = forcedLang;
        }
    } else {
        if (detectedLanguage.equals("en") || detectedLanguage.equals("es") || detectedLanguage.equals("ru") ||
                detectedLanguage.equals("fr") || detectedLanguage.equals("de") || detectedLanguage.equals("nl") || detectedLanguage.equals("sk")) {
            lang = detectedLanguage;
        }
    }

    String longLang = lang;

    switch(lang) {
        case "en":
            longLang = "eng";
            break;
        case "es":
            longLang = "spa";
            break;
        case "fr":
            longLang = "fre";
            break;
        case "de":
            longLang = "ger";
            break;
        case "nl":
            longLang = "dut";
            break;
        case "sk":
            longLang = "slo";
            break;
        default:
            longLang = "eng";
    }
    ResourceBundle bundle = org.georchestra._header.Utf8ResourceBundle.getBundle("_header.i18n.index", new Locale(lang));

    javax.servlet.jsp.jstl.core.Config.set(
            request,
            javax.servlet.jsp.jstl.core.Config.FMT_LOCALIZATION_CONTEXT,
            new javax.servlet.jsp.jstl.fmt.LocalizationContext(bundle)
    );

    Boolean extractor = true;
    Boolean admin = true;
    Boolean catadmin = true;
    Boolean console = true;
    Boolean analyticsadmin = true;
    Boolean extractorappadmin = true;
    Boolean msadmin = true;
    String sec_roles = request.getHeader("sec-roles");
    if (sec_roles != null) {
        String[] roles = sec_roles.split(";");
        for (int i = 0; i < roles.length; i++) {
            if (roles[i].equals("ROLE_GN_EDITOR") || roles[i].equals("ROLE_GN_REVIEWER") || roles[i].equals("ROLE_GN_ADMIN") || roles[i].equals("ROLE_ADMINISTRATOR") || roles[i].equals("ROLE_USER")) {
                anonymous = false;
            }
            if (roles[i].equals("ROLE_SUPERUSER")) {
                admin = true;
                console = true;
            }
            if (roles[i].equals("ROLE_ORGADMIN")) {
                admin = true;
                console = true;
            }
            if (roles[i].equals("ROLE_GN_ADMIN")) {
                admin = true;
                catadmin = true;
            }
            if (roles[i].equals("ROLE_ADMINISTRATOR")) {
                admin = true;
                extractorappadmin = true;
            }
            if (roles[i].equals("ROLE_MAPSTORE_ADMIN")) {
                admin = true;
                msadmin = true;
            }
        }
    }

%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <base target="_parent"/>
    <style type="text/css">
        /* see https://github.com/georchestra/georchestra/issues/147 for missing http protocol */
        @import url(//fonts.googleapis.com/css?family=Yanone+Kaffeesatz);

        html, body {
            padding: 0;
            margin: 0;
            background: #fff;
            font-size: 14px;
            font-weight: bold;
            font-family: 'IBM Plex Mono', monospace;
            text-transform: uppercase;
            color: #000;
            height: 70px;
            overflow: hidden;
        }

        header {
            margin-left: 30px;
            margin-right: 30px;
            height: 70px;
            line-height: 30px;
        }

        .grow {
            flex-grow: 1;
        }
        .flex {
            display: flex;
        }
        .flex-wrap {
            flex-wrap: wrap;
        }

        a {
            text-decoration: none;
            color: #000;
        }

        ul {
            list-style: none;
            align-content: center;
            align-items: center;
            justify-content: center;
            justify-items: center;
            transition: 0.5s all;
            overflow-wrap: break-word;
            margin: 0;
        }

        li > a:hover, .lang-switcher a:hover {
            border-bottom: 2px solid #6BC3E7;
            padding-bottom: 5px;
        }

        li > a {
            margin-right: 50px;
        }

        li.active a {
            color: #6BC3E7;
        }

        .logged {
            border: 1px dotted #ddd;
            padding: 0 0.6em;
        }
        .logged:hover {
            border: 1px dotted #6BC3E7;
        }
        .logged span {
            color: #000;
        }

        .logged span.light {
            color: #ddd;
        }

        .group > a:after {
            content: ' »';
        }

        .expanded > a:before {
            content: '« ';
        }

        .expanded > a:after {
            content: '';
        }

        ul ul {
            display: none;
        }

        .language {
            display: flex;
            justify-content: flex-end;
            flex-direction: row-reverse;
            transition: 0.5s all;
            align-content: center;
            align-items: center;
            justify-content: center;
            justify-items: center;
        }

        .lang-switcher, .logged {
            width: auto;
            margin: 0 15px 0 0;
        }

        .lang-switcher .active {
            color: #6BC3E7;
        }

        .lang-switcher a {
            color: #000;
        }

        @media (max-width: 1024px) {
            li > a {
                margin-right: 25px;
            }
        }

        @media (max-width: 748px) {
            header {
                line-height: 20px
            }
            li > a {
                margin-right: 10px;
            }
        }

    </style>

</head>


<body>
<header class="flex ">
    <div class="grow">
        <a href="/" title="<fmt:message key='go.home'/>">
            <img src="img/logo.png" style="margin-top: 10px;" alt="<fmt:message key='logo'/>" height="50"/>
        </a>
    </div>
    <ul class="apps flex flex-wrap">
        <c:choose>
            <c:when test='<%= active.equals("datahub") %>'>
                <li class="active"><a href="/datahub/"><fmt:message
                        key="catalogue"/></a></li>
            </c:when>
            <c:otherwise>
                <li><a href="/datahub/"><fmt:message key="catalogue"/></a>
                </li>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test='<%= active.equals("mapstore") %>'>
                <li class="active"><a><fmt:message key="mapstore"/></a></li>
            </c:when>
            <c:otherwise>
                <li><a href="/mapstore/"><fmt:message key="mapstore"/></a></li>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test='<%= active.equals("geoserver") %>'>
                <li class="active"><a href="/geoserver/web/"><fmt:message
                        key="services"/></a></li>
            </c:when>
            <c:otherwise>
                <li><a href="/geoserver/web/"><fmt:message key="services"/></a>
                </li>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test='<%= admin == true %>'>
                <li class="group">
                    <a href=""><fmt:message key="admin"/></a>
                </li>
            </c:when>
        </c:choose>
    </ul>
    <ul class="admin flex-wrap" style="display: none">
        <c:choose>
            <c:when test='<%= admin == true %>'>
                <li class="expanded">
                    <a href="#admin"><fmt:message key="admin"/></a>
                </li>
            </c:when>
        </c:choose>
        <c:choose>
            <c:when test='<%= catadmin == true %>'>
                <c:choose>
                    <c:when test='<%= active.equals("geonetwork") %>'>
                        <!-- GN2 or GN3 -->
                        <!--li class="active"><a href="/geonetwork/srv/<%= lang %>/admin"><fmt:message
                            key="catalogue"/></a></li-->
                        <li class="active"><a
                                href="/geonetwork/srv/<%= longLang %>/admin.console"><fmt:message
                                key="catalogue"/></a></li>
                    </c:when>
                    <c:otherwise>
                        <!-- GN2 or GN3 -->
                        <!--li><a href="/geonetwork/srv/<%= lang %>/admin"><fmt:message
                            key="catalogue"/></a></li-->
                        <li>
                            <a href="/geonetwork/srv/<%= longLang %>/admin.console"><fmt:message
                                    key="catalogue"/></a></li>
                    </c:otherwise>
                </c:choose>
            </c:when>
        </c:choose>

        <c:choose>
            <c:when test='<%= msadmin == true %>'>
                <c:choose>
                    <c:when test='<%= active.equals("msadmin") %>'>
                        <li class="active"><a><fmt:message
                                key="mapstore"/></a></li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="/mapstore/#/admin"><fmt:message
                                    key="mapstore"/></a></li>
                    </c:otherwise>
                </c:choose>
            </c:when>
        </c:choose>

        <c:choose>
            <c:when test='<%= extractorappadmin == true %>'>
                <c:choose>
                    <c:when test='<%= active.equals("extractorappadmin") %>'>
                        <li class="active"><a
                                href="/extractorapp/admin/"><fmt:message
                                key="extractor"/></a></li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="/extractorapp/admin/"><fmt:message
                                    key="extractor"/></a></li>
                    </c:otherwise>
                </c:choose>
            </c:when>
        </c:choose>

        <c:choose>
            <c:when test='<%= console == true %>'>
                <c:choose>
                    <c:when test='<%= active.equals("analytics") %>'>
                        <li class="active"><a
                                href="/analytics/">analytics</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="/analytics/">analytics</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:when>
        </c:choose>

        <c:choose>
            <c:when test='<%= console == true %>'>
                <c:choose>
                    <c:when test='<%= active.equals("console") %>'>
                        <li class="active"><a><fmt:message
                                key="users"/></a></li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="${consolePublicContextPath}/manager/"><fmt:message
                                    key="users"/></a></li>
                    </c:otherwise>
                </c:choose>
            </c:when>
        </c:choose>

    </ul>
    <div class="language flex flex-wrap">
        <c:choose>
            <c:when test='<%= anonymous == false %>'>
                <p class="logged">
                    <a href="${consolePublicContextPath}/account/userdetails"><%=request.getHeader("sec-username") %>
                    </a><span class="light"> | </span><a
                        href="/logout"><fmt:message key="logout"/></a>
                </p>
            </c:when>
            <c:otherwise>
                <p class="logged">
                    <a id="login_a"><fmt:message key="login"/></a>
                </p>
            </c:otherwise>
        </c:choose>
        <div class="lang-switcher">
            <a onclick="return georSwitchLang('sk')"
               class='<%=lang.equals("sk") ? "active" : ""%>' href="">SK</a> |
            <a onclick="return georSwitchLang('en')"
               class='<%=lang.equals("en") ? "active" : ""%>' href="">EN</a>
        </div>
    </div>
</header>

<script>
  (function () {
    // required to get the correct redirect after login, see https://github.com/georchestra/georchestra/issues/170
    var url,
      a = document.getElementById("login_a"),
      cnxblk = document.querySelector('p.logged');
    if (a !== null) {
      url = parent.window.location.href;
      if (/\/cas\//.test(url)) {
        a.href = "/cas/login";
      } else {
        // removing any existing anchor from URL first:
        // see https://github.com/georchestra/georchestra/issues/1032
        var p = url.split('#', 2),
          /* Taken from https://github.com/openlayers/openlayers/blob/master/lib/OpenLayers/Util.js#L557 */
          paramStr = "login", parts = (p[0] + " ").split(/[?&]/);
        a.href = p[0] + (parts.pop() === " " ?
            paramStr :
            parts.length ? "&" + paramStr : "?" + paramStr) +
          // adding potential anchor
          (p.length == 2 ? "#" + p[1] : "");
      }
    }

    // handle menus
    if (!window.addEventListener || !document.querySelectorAll) return;
    var each = function (els, callback) {
      for (var i = 0, l = els.length; i < l; i++) {
        callback(els[i]);
      }
    }

    const toggleAdmin = function(e) {
      const ulApps = document.querySelector('ul.apps')
      const ulAdmin = document.querySelector('ul.admin')
      if (ulApps.style.display === 'none') {
        ulApps.style.display = 'flex'
        ulAdmin.style.display = 'none'
      } else {
        ulApps.style.display = 'none'
        ulAdmin.style.display = 'flex'
      }
        e.stopImmediatePropagation();
        e.stopPropagation();
        e.preventDefault();
        return false;
    }
    document.querySelector('li.group a').addEventListener('click', toggleAdmin)
    document.querySelector('li.expanded a').addEventListener('click', toggleAdmin)
  })();

  function georSwitchLang(lang) {
    document.cookie = "geor-lang=" + lang + ";path=/";
    window.top.location.reload();
    return false;
  }
</script>

</body>
</html>
