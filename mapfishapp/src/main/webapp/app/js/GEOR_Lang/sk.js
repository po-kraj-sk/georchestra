/*
* @requires GeoExt/Lang.js
*/

/*
* Slovak translation file
*/
OpenLayers.Lang.sk = OpenLayers.Util.extend(OpenLayers.Lang.en, {
  /* General purpose strings */
  "Yes": "Áno",
  "No": "Nie",
  "OK": "OK",
  "or": "alebo",
  "Cancel": "Zrušiť",
  "Save": "Uložiť",
  "Loading...": "Načítava sa...",
  "File": "Súbor",
  "Layer": "Vrstva",
  "layers": "vrstvy",
  "Description": "Popis",
  "Error": "Chyba",
  "Server": "Server",
  "Close": "Zavrieť",
  "labelSeparator": " : ",
  "File submission failed or invalid file": "Odoslanie súboru zlyhalo alebo je súbor neplatný",
  "Type": "Typ",
  "Title": "Názov",
  "Actions": "Akcie",
  "Incorrect server response.": "Nesprávna odpoveď servera.",
  "No features found.": "Nebol nájdený žiaden objekt.",
  "Reload": "Znova načítať",
  /* GEOR.js strings */
  "Cities": "Mestá",
  "Recentering on GeoNames cities": "Priblížiť na mestá<br />z databázy GeoNames",
  "Referentials": "Referenčné vrstvy",
  "Recentering on a selection of referential layers": "Priblížiť na vybranú referenčnú vrstvu<br />vrstvy \"referenčné vrstvy\"",
  "Addresses": "Adresy",
  "Recentering on a given address": "Priblížiť na zadanú adresu",
  "Available layers": "Dostupné vrstvy",
  "WMS Search": "Vyhľadať službu WMS",
  "WFS Search": "Vyhľadať službu WFS",
  "resultspanel.emptytext":
    "<p>Vybrať nástroj na vyhľadávanie" +
    "alebo vytvoriť dopyt na vrstve.<br />" +
    "Atribúty objektov sa zobrazia v tomto rámčeku.</p>",
  /* GEOR_ClassificationPanel.js strings */
  "Attribute": "Atribút",
  "Number of classes": "Počet tried",
  "Minimum size": "Minimálna veľkosť",
  "Maximum size": "Maximálna veľkosť",
  "First color": "Prvá farba",
  "Last color": "Posledná farba",
  "Palette": "Paleta",
  "Auto classification": "Automatická klasifikácia",
  "Classify": "Klasifikovať",
  "Unique values": "Jedinečné hodnoty",
  "Color range": "Rozsah farieb",
  "Proportional symbols": "Proporcionálne symboly",
  /* GEOR_FeatureDataModel.js strings */
  "objects": "objekty",
  /* GEOR_address.js strings */
  "Go to: ": "Ísť na: ",
  "searching...": "vyhľadáva sa...",
  "adressSearchExemple": "napr. Hlavná 73, Prešov",
  /* GEOR_ajaxglobal.js strings strings */
  "Server did not respond.": "Server neodpovedal.",
  "Server access denied.": "Prístup k serveru bol zamietnutý.",
  "ajax.badresponse":
    "Služba odpovedala, ale obsah" +
    "odpovede nie je v súlade s očakávaným výsledkom",
  "Server unavailable.": "Server je dočasne nedostupný. Skústo to neskôr znova.",
  "Too much data.": "Príliš veľa dát.",
  "Server exception.": "Server vrátil chybu.",
  "ajax.defaultexception":
    "Pre získanie viac informácií môžete vyhľadať" +
    "chybový kód <a href=\"http://" +
    "en.wikipedia.org/wiki/List_of_HTTP_status_codes\" target=\"_blank\">" +
    "na tejto stránke</a>.",
  "An error occured.<br />": "Vyskytla sa chyba.<br />",
  "Warning : browser may freeze": "Upozornenie: prehliadač nemusí fungovať správne",
  "ajaxglobal.data.too.big": "Údaje odoslané zo servera" +
    "sú príliš veľké.<br />Server odoslal ${SENT}KBytes " +
    "(limit je ${LIMIT}KBytes)<br />Prajete si pokračovať?",
  /* GEOR_config.js strings */
  /* GEOR_cswbrowser.js strings */
  "NAME layer": "Vrstva ${NAME}",
  "Metadata without a name": "Metadáta bez názvu",
  "The getDomain CSW query failed": "Dopyt po požiadavke CSW getDomain zlyhal",
  "Error for the thesaurus": "Chyba tezauru",
  "Missing key to access the thesaurus":
    "Chýba kľúč na prístup k tezauru",
  "Keywords query failed": "Dopyt po kľúčových slovách zlyhal",
  "Thesaurus:": "Tezaurus:",
  "Thesaurus": "Tezaurus",
  "cswbrowser.default.thesaurus.mismatch":
    "Administrátor: problém v konfigurácii - " +
    "premenná DEFAULT_THESAURUS_KEY sa nezhoduje" +
    "so žiadnou hodnotu exportovanou na GeoNetwork",
  /* GEOR_cswquerier.js strings */
  "cswquerier.help.title": "Návod pre pokročilé dopytovanie",
  "cswquerier.help.message": "<ul><li><b>@slovo</b> vyhľadávanie podľa mena organizácie.</li><li><b>#slovo</b> vyhľadávanie v kľúčových slovách metadát.</li><li><b>?mot</b> rozšírené vyhľadávanie na všetky polia metadát.</li></ul>",
  "NAME layer on VALUE": "Vrstva ${NAME} s ${VALUE}",
  "Show metadata essentials in a window":
    "Zobraziť základné informácie o metadátach v okne",
  "Show metadata sheet in a new browser tab":
    "Zobraziť metadáta v novom okne prehliadača",
  "more": "viac",
  "Click to select or deselect the layer":
    "Kliknutím vybrať alebo zrušiť výber vrstvy",
  "Open the URL url in a new window":
    "Otvoriť URL adresu v novom okne",
  "Unreachable server": "Server nie je k dispozícii",
  "Catalogue": "Katalóg",
  "Find": "Nájsť",
  "in": "v",
  "No linked layer.": "Nie je pripojená žiadna vrstva.",
  "One layer found.": "Nájdená jedna vrstva.",
  "NB layers found.": "Vrstvy ${NB} nájdené.",
  "NB metadata match the query.": "Metadáta ${NB} zodpovedajú dopytu.",
  "A single metadata matches the query.": "Jednotlivé metadáta zodpovedajú dopytu.",
  "Precise your request.": "Zadajte svoju požiadavku.",
  "No metadata matches the query.":
    "Dopytu nezodpovedajú žiadne metadáta.",
  "Limit to map extent": "Obmedziť rozsah mapového okna",
  "Search limited to current map extent.": "Vyhľadávanie je obmedzené na rozsah mapového okna.",
  /* GEOR_fileupload.js strings */
  "2D only": "len 2D",
  "Local file": "Súbor",
  "The service is inactive": "Služba je neaktívna",
  "Upload a vector data file.": "Nahrať vektorový súbor.",
  "The allowed formats are the following: ": "Povolené formáty: ",
  "Use ZIP compression for multifiles formats, such as": "Použite prosím ZIP kompresiu pre viacsúborové formáty, ako je",
  "fileupload_error_incompleteMIF": "Súbor MIF/MID neúplný.",
  "fileupload_error_incompleteSHP": "Súbor shapefile neúplný.",
  "fileupload_error_incompleteTAB": "Súbor TAB neúplný.",
  "fileupload_error_ioError": "Chyba I/O na serveri. Kontaktujte prosím administrátora platformy.",
  "fileupload_error_multipleFiles": "Archív ZIP obsahuje niekoľko dátových súborov. Musí obsahovať iba jeden.",
  "fileupload_error_outOfMemory": "Server nemá dostatok pamäte. Kontaktujte prosím administrátora platformy.",
  "fileupload_error_sizeError": "Súbor je príliš veľký na spracovanie.",
  "fileupload_error_unsupportedFormat": "Súbor nie je podporovaný.",
  "fileupload_error_projectionError": "Počas načítavania geografických súradníc sa vyskytla chyba. Ste si istý, že súbor obsahuje informáciu o projekcii?",
  "server upload error: ERROR":
    "Nahrávanie súboru zlyhalo. ${ERROR}",
  /* GEOR_geonames.js strings */
  /* GEOR_getfeatureinfo.js strings */
  "<div>Searching...</div>": "<div>Vyhľadáva sa...</div>",
  "<div>No layer selected</div>": "<div>Nie je vybraná žiadna vrstva</div>",
  "<div>Search on objects active for NAME layer. Click on the map.</div>":
    "<div>Vyhľadávanie na objektoch aktívnych pre vrstvu ${NAME}." +
    "Kliknúť na mapu.</div>",
  "WMS GetFeatureInfo at ": "Požiadavka WMS GetFeatureInfo na ",
  /* GEOR_layerfinder.js strings */
  "metadata": "metadáta",
  "Add layers from local files": "Pridať vrstvy zo súborov",
  "Find layers searching in metadata":
    "Nájsť vrstvy vyhľadávaním v metadátach",
  "Find layers from keywords": "Nájsť vrstvy z kľúčových slov",
  "Find layers querying OGC services":
    "Nájsť vrstvy dopytovaním služieb OGC",
  "layerfinder.layer.unavailable":
    "V službe WMS sa nepodarilo nájsť vrstvu $ {NAME}.<br/<br/>" +
    "Na danú operáciu pravdepodobne nemáte prístup" +
    "alebo táto vrstva už nie je k dispozícii",
  "Layer projection is not compatible":
    "Projekcia vrstvy nie je kompatibilná.",
  "The NAME layer does not contain a valid geometry column":
    "Vrstva $ {NAME} nemá stĺpec s platnou geometriou.",
  "Add": "Pridať",
  "Add layers from a ...": "Pridať vrstvy z...",
  "Malformed URL": "Nesprávna URL adresa.",
  "Queryable": "Dopytovateľné",
  "Opaque": "Nezrozumiteľné",
  "OGC server": "Server OGC",
  "I'm looking for ...": "Hľadám...",
  "Service type": "Typ služby",
  "Choose a server": "Vybrať server",
  "... or enter its address": "...alebo zadať jeho adresu",
  "The server is publishing one layer with an incompatible projection":
    "Server publikuje jednu vrstvu s nekompatibilnou projekciou",
  "The server is publishing NB layers with an incompatible projection":
    "Server publikuje vrstvy ${NB} s nekompatibilnou projekciou" +
    "kompatibilnou",
  "This server does not support HTTP POST": "Tento server nepodporuje protokol HTTP POST",
  "Unreachable server or insufficient rights": "Neplatná odpoveď" +
    "servera. Možné dôvody: nedostatočné práva," +
    "server je nedostupný, príliš veľa údajov a pod.",
  /* GEOR_managelayers.js strings */
  "layergroup": "skupina vrstiev",
  "Service": "Služba",
  "Protocol": "Protokol",
  "About this layer": "O tejto vrstve",
  "Set as overlay": "Nastaviť ako prekryv",
  "Set as baselayer": "Nastaviť ako podkladovú vrstvu",
  "Tiled mode": "Režim dlaždíc",
  "Confirm NAME layer deletion ?":
    "Naozaj si prajete vymazať vrstvu ${NAME}?",
  "1:MAXSCALE to 1:MINSCALE": "1:${MAXSCALE} à 1:${MINSCALE}",
  "Visibility range (indicative):<br />from TEXT":
    "Rozsah viditeľnosti (orientačný):<br />de ${TEXT}",
  "Information on objects of this layer":
    "Informácie o objektoch tejto vrtsvy",
  "default style": "predvolený štýl",
  "no styling": "bez štýlu",
  "Recenter on the layer": "Priblížiť na vrstvu",
  "Impossible to get layer extent":
    "Nie je možné získať rozsah vrstvy.",
  "Refresh layer": "Obnoviť vrstvu",
  "Show metadata": "Zobraziť metadáta",
  "Edit symbology": "Upraviť symbológiu",
  "Build a query": "Vytvoriť dopyt",
  "Download layer": "Stiahnuť vrstvu",
  "Extract data": "Extrahovať dáta",
  "Choose a style": "Vybrať štýl",
  "Modify format": "Zmeniť formát",
  "Delete this layer": "Vymazať túto vrstvu",
  "Push up this layer": "Presunúť nahor túto vrstvu",
  "Push down this layer": "Presunúť nadol túto vrstvu",
  "Add layers": "Pridať vrstvy",
  "Remove all layers": "Odstrániť všetky vrstvy",
  "Are you sure you want to remove all layers ?": "Naozaj chcete odstrániť všetky vrstvy?",
  "source: ": "zdroj: ",
  "unknown": "neznámy",
  "Draw new point": "Zakresliť nový bod",
  "Draw new line": "Zakresliť novú líniu",
  "Draw new polygon": "Zakresliť nový polygón",
  "Edition": "Editácia",
  "Editing": "Editovanie",
  "Switch on/off edit mode for this layer": "Zapnúť/Vypnúť editovací mód pre túto vrstvu",
  "No geometry column.": "Žiadny stĺpec s geometriou.",
  "Geometry column type (TYPE) is unsupported.": "Stĺpec s typom geometrie (TYPE) nie je podporovaný.",
  "Switching to attributes-only edition.": "Editovateľné budú iba atribúty existujúcich objektov.",
  /* GEOR_map.js strings */
  "Location map": "Poloha mapy",
  "Warning after loading layer":
    "Upozornenie po načítaní vrstvy",
  "The <b>NAME</b> layer could not appear for this reason: ":
    "Vrstva <b>${NAME}</b> sa nezobrazila" +
    "z tohto dôvodu : ",
  "Min/max visibility scales are invalid":
    "Mierky viditeľnosti min / max sú neplatné.",
  "Visibility range does not match map scales":
    "Rozsah viditeľnosti sa nezhoduje s mierkou mapy.",
  "Geografic extent does not match map extent":
    "Geografický rozsah sa nezhoduje s rozsahom mapy.",
  /* GEOR_mapinit.js strings */
  "Add layers from WMS services":
    "Pridať vrstvy zo služieb WMS",
  "Add layers from WFS services":
    "Pridať vrstvy zo služieb WFS",
  "NB layers not imported": "Vrstvy ${NB} sa nepodarilo importovať",
  "One layer not imported": "Jednu vrstvu sa nepodarilo importovať",
  "mapinit.layers.load.error":
    "Vrstvy s názvom ${LIST} nie je možné načítať." +
    "Možné dôvody: nedostatočné práva, nekompatibilná projekcia alebo neexistujúca vrstva",
  "NB layers imported": "Boli importované vrstvy ${NB}",
  "One layer imported": "Bola importovaná jedna vrstva",
  "No layer imported": "Nebola importovaná žiadna vrstva",
  "The provided context is not valid": "Poskytovaný projekt je neplatný",
  "The default context is not defined (and it is a BIG problem!)":
    "Predvolený projekt nie je zadefinovaný" +
    "(a to je veľký problém!)",
  "Error while loading file": "Chyba pri načítavaní súboru",
  /* GEOR_mappanel.js strings */
  "Coordinates in ": "Súradnice v",
  "scale picker": "mierka",
  /* GEOR_ows.js strings */
  "The NAME layer was not found in WMS service.":
    "V službe WMS nebola nájdená vrstva ${NAME}.",
  "Problem restoring a context saved with buggy Chrome 36 or 37":
    "Problém s obnovením projektu uloženého v Chrome 36 alebo 37",
  /* GEOR_print.js strings */
  "Sources: ": "Zdroje: ",
  "Source: ": "Zdroj: ",
  "Projection: PROJ": "Projekcia: ${PROJ}",
  "Print error": "Chyba tlače",
  "Print server returned an error":
    "Server pri tlači vrátil chybu.",
  "Contact platform administrator":
    "Kontaktujte prosím administrátora platformy.",
  "Layer unavailable for printing": "Vrstva nie je k dispozícii pre tlač",
  "The NAME layer cannot be printed.":
    "Vrstvu ${NAME} nie je možné vytlačiť.",
  "Unable to print": "Nie je možné tlačiť",
  "The print server is currently unreachable":
    "Server je momentálne pre tlač nedostupný",
  "print.unknown.layout":
    "Chyba pri konfigurácii: DEFAULT_PRINT_LAYOUT" +
    "$ {LAYOUT} nie je v zozname tlačových formátov",
  "print.unknown.resolution":
    "Chyba pri konfigurácii: DEFAULT_PRINT_RESOLUTION" +
    "$ {RESOLUTION} sa nenachádza v zozname rozlíšení tlače",
  "print.unknown.format":
    "Chyba pri konfigurácii: formát" +
    "$ {FORMAT} nie je podporovaný serverom pri tlači",
  "Pick an output format": "Vybrať formát výstupu",
  "Comments": "Komentáre",
  "Scale: ": "Mierka: ",
  "Date: ": "Dátum: ",
  "Minimap": "Mini mapa",
  "North": "Sever",
  "Scale": "Mierka",
  "Date": "Dátum",
  "Legend": "Legenda",
  "Format": "Formát",
  "Resolution": "Rozlíšenie",
  "Print the map": "Vytlačiť mapu",
  "Print": "Tlač",
  "Printing...": "Tlačí sa...",
  "Print current map": "Vytlačiť aktuálnu mapu",
  /* GEOR_Querier.js strings */
  "Fields of filters with a red mark are mandatory": "Vyplňte prosím" +
    "polia označené červenou farbou.",
  "Request on NAME": "Požiadavka na ${NAME}",
  "WFS GetFeature on filter": "Požiadavka WFS GetFeature na filter",
  "Search": "Vyhľadať",
  "querier.layer.no.geom":
    "Vrstva nemá stĺpec s geometriou." +
    "<br />Požiadavka na geometriu nie je platná.",
  "querier.layer.error":
    "Nepodarilo sa získať vlastnosti požadovanej vrstvy." +
    "<br />Požiadavka nie je k dispozícii.",
  /* GEOR_referentials.js strings */
  "Referential": "Referenčná vrstva",
  "There is no geometry column in the selected referential":
    "Vo vybranej referenčnej vrstve nie je stĺpec s geometriou",
  "Choose a referential": "Vybrať referenčnú vrstvu",
  /* GEOR_resultspanel.js strings */
  "Symbology": "Symbológia",
  "Edit this panel's features symbology": "Upraviť symbológiu objektov tohto panelu",
  "Reset": "Resetovať",
  "Export is not possible: features have no geometry": "Export nie je možný: objekty nemajú geometriu",
  "resultspanel.maxfeature.reached":
    " <span ext:qtip=\"Použite prosím iný prehliadač" +
    "Zvýšte počet zobrazovaných objektov\">" +
    "Maximálny počet zobrazovaných objektov (${NB})</span>",
  "NB results": "Výsledky pre ${NB}",
  "One result": "1 výsledok",
  "No result": "Žiadny výsledok",
  "Clean": "Vyčistiť",
  "All": "Všetky",
  "None": "Žiadne",
  "Invert selection": "Opačný výber",
  "Actions on the selection or on all results if no row is selected":
    "Ak nie je vybraný žiadny riadok, vykonajte akcie týkajúce sa výberu alebo všetkých výsledkov",
  "Store the geometry":
    "Uložiť geometriu",
  "Aggregates the geometries of the selected features and stores it in your browser for later use in the querier":
    "Geometria vybraných objektov sa uloží v prehliadači pre neskoršie použitie v dopytovacom nástroji",
  "Geometry successfully stored in this browser":
    "Geometria bola úspešne uložená v tomto prehliadači",
  "Clean all results on the map and in the table": "Vyčistiť všetky" +
    "výsledky zobrazené na mape a v tabuľke",
  "Zoom": "Priblížiť",
  "Zoom to results extent": "Priblížiť na rozsah" +
    "výsledkov",
  "Export": "Export",
  "Export results as": "Exportovať výsledky ako",
  "<p>No result for this request.</p>": "<p>Žiadny výsledok" +
    "pre zadanú požiadavku.</p>",
  /* GEOR_scalecombo.js strings */
  /* GEOR_selectfeature.js strings */
  "<div>Select features activated on NAME layer. Click on the map.</div>":
    "<div>Vybrať d\'objekty aktívne vo vrstve ${NAME}. " +
    "Kliknúť na mapu.</div>",
  "OpenLayers SelectFeature": "Vybrať d\'objekty",
  /* GEOR_styler.js strings */
  "Download style": "Stiahnuť štýl",
  "You can download your SLD style at ": "Štýl SLD je možné stiahnúť" +
    "l\'na adrese: ",
  "Thanks!": "Ďakujem!",
  "Saving SLD": "Ukladá sa štýl SLD",
  "Some classes are invalid, verify that all fields are correct": "Niektoré" +
    "triedy sú neplatné, skontrolujte, či sú všetky polia správne",
  "Get SLD": "Získať štýl SLD",
  "Malformed SLD": "Nesprávny štýl SLD",
  "circle": "kruh",
  "square": "štvorec",
  "triangle": "trojuholník",
  "star": "hviezda",
  "cross": "kríž",
  "x": "x",
  "customized...": "prispôsobené...",
  "Classification ...<br/>(this operation can take some time)":
    "Klasifikácia ...<br/>(táto operácia môže chvíľu trvať)",
  "Class": "Trieda",
  "Untitled": "Bez názvu",
  "styler.guidelines":
    "Použite tlačidlo \"+\" na vytvorenie triedy, tlačidlo" +
    "\"Analýza\" na vytvorenie súborov tried podľa" +
    "tematickej analýzy.</p>",
  "Analyze": "Analyzovať",
  "Add a class": "Pridať triedu",
  "Delete the selected class": "Vymazať vybranú triedu",
  "Styler": "Štýlovanie",
  "Apply": "Použiť",
  "Impossible to complete the operation:": "Nie je možné dokončiť operáciu:",
  "no available attribute": "atribút nie je k dispozícii",
  /* GEOR_toolbar.js strings */
  "m": "m",
  "hectares": "hektáre",
  "zoom to global extent of the map": "priblížiť na celkový rozsah" +
    "mapy",
  "pan": "posunúť",
  "zoom in": "priblížiť",
  "zoom out": "oddialiť",
  "back to previous zoom": "návrat k predchádzajúcemu pohľadu",
  "go to next zoom": "prejsť na nasledujúci pohľad",
  "Login": "Prihlásiť sa",
  "Logout": "Odhlásiť sa",
  "Help": "Pomoc",
  "Query all active layers": "Dopytovať všetky aktívne vrstvy",
  "Show legend": "Zobraziť legendu",
  "Leave this page ? You will lose the current cartographic context.":
    "Opustením tejto stránky stratíte aktuálny kartografický rozsah.",
  "Online help": "Online pomoc",
  "Display the user guide": "Zobraziť používateľskú príručku",
  "Contextual help": "Kontextová pomoc",
  "Activate or deactivate contextual help bubbles": "Aktivácia alebo deaktivácia kontextovej pomoci",
  /* GEOR_tools.js strings */
  "Tools": "Nástroje",
  "tools": "nástroje",
  "tool": "nástroj",
  "No tool": "Žiaden nástroj",
  "Manage tools": "Spravovať nástroje",
  "remember the selection": "zapamätať si výber",
  "Available tools:": "Dostupné nástroje:",
  "Click to select or deselect the tool": "Kliknutím vybrať alebo zrušiť výber nástroja",
  "Could not load addon ADDONNAME": "Nebolo možné načítať addon ${ADDONNAME}",
  "Your new tools are now available in the tools menu.": 'Vaše nové nástroje sú teraz dostupné v ponuke "nástrojov"',
  /* GEOR_util.js strings */
  "Characters": "Znaky",
  "Digital": "Digitálne",
  "Boolean": "Booleovské",
  "Other": "Iné",
  "Confirmation": "Potvrdenie",
  "Information": "Informácia",
  "pointOfContact": "kontakt",
  "custodian": "producent",
  "distributor": "distribútor",
  "originator": "pôvodca",
  "More": "Viac",
  "Could not parse metadata.": "Nie je možné analyzovať metadáta",
  "Could not get metadata.": "Nie je možné získať metadáta",
  /* GEOR_waiter.js strings */
  /* GEOR_wmc.js strings */
  "The provided file is not a valid OGC context": "Poskytnutý súbor nie je platným projektom OGC",
  "Warning: trying to restore WMC with a different projection (PROJCODE1, while map SRS is PROJCODE2). Strange things might occur !": "Upozornenie: pokúste sa zmeniť projekciu súboru WMC ${PROJCODE1}, pretože mapa má projekciu ${PROJCODE2}. Mohli by sa vyskytnúť neočakávané veci!",
  /* GEOR_wmcbrowser.js strings */
  "all contexts": "všetky projekty",
  "Could not find WMC file": "Nepodarilo sa nájsť súbor WMC",
  "... or a local context": "...alebo projekt uložený v počítači",
  "Load or add the layers from one of these map contexts:": "Načítať alebo pridať vrstvy z jedného z týchto mapových projektov:",
  "A unique OSM layer": "Jedna vrstva OpenStreetMap",
  "default viewer context": "predvolený projekt",
  "(default)": "<br/>(predvolený)",
  /* GEOR_workspace.js strings */
  "Created:": "Vytvorený: ",
  "Last accessed:": "Dátum posledného prístupu: ",
  "Access count:": "Počet prístupov: ",
  "Permalink:": "Permalink: ",
  "My contexts": "Moje projekty",
  "Created": "Vytvorené",
  "Accessed": "Prístupné",
  "Count": "Počet",
  "View": "Zobraziť",
  "View the selected context": "Zobraziť vybraný projekt (poznámka: nahradí aktuálny projekt)",
  "Download": "Stiahnuť",
  "Download the selected context": "Stiahnuť vybraný projekt",
  "Delete": "Vymazať",
  "Delete the selected context": "Vymazať vybraný projekt",
  "Failed to delete context": "Nepodarilo sa vymazať projekt",
  "Manage my contexts": "Spravovať moje projekty",
  "Keywords": "Kľúčové slová",
  "comma separated keywords": "čiarkou oddelené kľúčové slová",
  "Save to metadata": "Uložiť do metadát",
  "in group": "v skupine",
  "The context title is mandatory": "Názov projektu je povinný",
  "There was an error creating the metadata.": "Pri vytváraní metadát sa vyskytla chyba.",
  "Share this map": "Zdieľať túto mapu",
  "Mobile viewer": "Mobilný prehliadač",
  "Mobile compatible viewer": "Kompatibilný mobilný prehliadač",
  "Desktop viewer": "Desktopový prehliadač",
  "Desktop viewer": "Desktopový prehliadač",
  "Abstract": "Abstrakt",
  "Context saving": "Projekt sa ukladá",
  "The file is required.": "Súbor je povinný.",
  "Context restoring": "Projekt sa obnovuje",
  "<p>Please note that the WMC must be UTF-8 encoded</p>": "<p>Upozorňujeme," +
    "že súbor MWC musí mať kódovanie UTF-8.</p>",
  "Load": "Načítať",
  "Workspace": "Pracovná plocha",
  "Save the map context": "Uložiť mapový projekt",
  "Load a map context": "Načítať mapový projekt",
  "Get a permalink": "Získať permalink",
  "Permalink": "Permalink",
  "Share your map with this URL: ": "Zdieľať mapu s touto URL adresou: ",
  /* GEOR_edit.js */
  "Req.": "Pož.", // requis
  "Required": "Povinné",
  "Not required": "Nepovinné",
  "Synchronization failed.": "Synchronizácia zlyhala.",
  "Edit activated": "Editovanie aktivované",
  "Hover the feature you wish to edit, or choose \"new feature\" in the edit menu": "Priblížiť kurzor na objekt, ktorý si želáte upraviť, alebo vybrať \"nový objekt\" z ponuky na úpravy",
  /* GeoExt.data.CSW.js */
  "no abstract": "bez abstraktu"
// no trailing comma
});

GeoExt.Lang.add("sk", {
  "GeoExt.ux.FeatureEditorGrid.prototype": {
    deleteMsgTitle: "Vymazať",
    deleteMsg: "Potvrďte prosím vymazanie tohto vektorového objektu",
    deleteButtonText: "Vymazať",
    deleteButtonTooltip: "Vymazať tento objekt",
    cancelMsgTitle: "Zrušiť",
    cancelMsg: "Objekt bol lokálne upravený. Potvrďte prosím prerušenie zmien",
    cancelButtonText: "Zrušiť",
    cancelButtonTooltip: "Prerušiť prebiehajúce zmeny",
    saveButtonText: "Uložiť",
    saveButtonTooltip: "Uložiť zmeny",
    nameHeader: "Atribút",
    valueHeader: "Hodnota"
  }
});