#!/bin/sh
set -e

# cd /usr/share/nginx/html/

if [ "$GOOGLE_ANALYTICS_KEY" ]
then
  tee /tmp/googleanalytics-block-js.txt << EOF
  <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=$GOOGLE_ANALYTICS_KEY"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '$GOOGLE_ANALYTICS_KEY');
    </script>
EOF

  # Insert the content of the googleanalytics-block-js.txt file before </body>
  sed -i '/<\/body>/ {
    r /tmp/googleanalytics-block-js.txt
    N
  }' /var/lib/jetty/webapps/mapfishapp/WEB-INF/jsp/index.jsp

  # cleanup
  rm /tmp/googleanalytics-block-js.txt
fi


# run docker command
exec "$@"
